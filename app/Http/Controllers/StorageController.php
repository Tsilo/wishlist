<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\File;
use Dropbox;

class StorageController extends Controller
{
    //dropbox client
    private $dbxClient;

    public function __construct()
    {
        $this->dbxClient = new Dropbox\Client(env('DROPBOX_TOKEN'), 'laravel');
    }

    /**
     * get local files to display
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        echo storage_path('app/public');

        //$files = $this->dbxClient->getMetadataWithChildren('/');
        $localFiles = File::all();
        //$localFiles = Storage::allFiles('public');

        /* echo '<pre>';
         var_dump($files['contents'][0]);
         echo '</pre>';*/
        return view('storage.show', ['localFiles' => $localFiles]);
    }

    /**
     * @param $directory dropbox directory to fetch
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDropboxFiles(Request $request)
    {
        $directory = $request->input('directory');
        $files = $this->dbxClient->getMetadataWithChildren($directory);
        return response()->json($files);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocalFiles(){
        $localFiles = File::all();
        return response()->json($localFiles);
    }

    /**
     * save files to local from dropbox
     * @param mixed []
     * @return string
     */
    public function saveFiles(Request $request)
    {
        if ($request->isMethod('post')) {
            $errors = [];
            $filename = $request->input('files');
            $localPath = substr($filename, 0, strrpos($filename, '/') + 1);
            $name = substr($filename, strrpos($filename, '/') + 1);
            $path = $this->dbxClient->createTemporaryDirectLink($filename);
            //foreach($files as $file) {
            //download remote file to local app in /storage/app/public -> public/storage
            $stored = Storage::disk('public')->put($filename, fopen($path[0], 'r'), 'public');
            //store to database
            $db = new File;
            $db->filename = $name;
            $db->path = $localPath;
            $db->save();

            if (!$stored) {
                $errors[] = $name . ' file wasn\'t uploaded';
            }
            //}
            return response()->json([
                'file' => ['filename' => $name, 'path' => $localPath, 'id' => $db->id],
                'error' => $errors
            ]);
        }
        return false;


    }

    /**
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function deleteFile(Request $request){

        if ($request->isMethod('delete')) {
            $errors = [];
            $path = $request->input('file');
            $id = $request->input('id');
            $delete = Storage::disk('public')->delete($path);
            if(!Storage::disk('public')->exists($path)){
                File::destroy($id);
            }
            if (!$delete) {
                $errors[] = 'Error during delete';
            }
            //}
            return response()->json([
                'error' => $errors
            ]);
        }
        return false;
    }

    public function download($path){

        $path = storage_path('app/public/'.$path);

        return response()->download($path);
    }
}
