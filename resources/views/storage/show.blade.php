@extends('layouts.app')

@section('content')
    <div class="container" id="main-container">
        <div class="row">
            {{-- <div class="col-md-3" id="dropbox-chooser"></div>--}}


            <div class="col-md-6">

                <h3>Dropbox Files</h3>
                <dropbox-files></dropbox-files>
            </div>
            <div class="col-md-6">
                <h3>Local Files</h3>
                <local-files></local-files>
            </div>
        </div>
    </div>
@endsection
