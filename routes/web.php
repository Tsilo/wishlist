<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'StorageController@index');
Route::get('/dropboxfiles', 'StorageController@getDropboxFiles');
Route::get('/localfiles', 'StorageController@getLocalFiles');
Route::get('/download/{path}', 'StorageController@download')->where('path', '(.*)');
Route::post('/saveFile', 'StorageController@saveFiles');
Route::delete('/deleteFile', 'StorageController@deleteFile');

Auth::routes();

Route::get('/home', 'HomeController@index');
